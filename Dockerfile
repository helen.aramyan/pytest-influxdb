FROM python:3.8-slim

RUN pip3 install --upgrade pip && \
    pip3 install pytest==5.2.2 influxdb==5.2.3 keyring==15.1.0 twine==3.0.0

WORKDIR /app
ADD . /app