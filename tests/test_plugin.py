from pytest_influxdb.data_manager import DataManager
from pytest_influxdb.suite_result_dto import SuiteResultDTO
from pytest_influxdb.test_result_dto import TestResultDTO


def test_user_properties(request):
    DataManager().save_db_data_in_properties(request, "test1", "test2", "test3", "test4",
                                             "test4", "test5", "test6", "test7", "test8",
                                             "test9")
    assert request.node.user_properties[0] == {
        'influxdb_creds': {'influxdb_host': 'test1', 'influxdb_port': 'test2', 'influxdb_user': 'test3',
                           'influxdb_password': 'test4', 'influxdb_name': 'test4'},
        'test_values': {'influxdb_values': 'test5', 'run': 'test6', 'project': 'test7', 'version': 'test8',
                        'merged': 'test9'}}


def test_suite_custom_values():
    suite_results_dto = SuiteResultDTO()
    custom_values = {
        "fields": {
            "test_result": {
            },
            "suite_result": {
                'test_value1': 'test1'
            }
        },
        "tags": {
            "test_result": {
            },
            "suite_result": {
                'test_value2': 'test2',
                'test_value3': 'test3'
            }
        }
    }
    suite_results_dto.set_suite_custom_values(custom_values)
    assert suite_results_dto.get_suite_json('test_measurement_name')[0] == {'measurement': 'test_measurement_name',
                                                                            'tags': {'run': 'UNDEFINED',
                                                                                     'project': 'UNDEFINED',
                                                                                     'version': 'UNDEFINED',
                                                                                     'test_value2': 'test2',
                                                                                     'test_value3': 'test3'
                                                                                     },
                                                                            'fields': {'pass': None, 'fail': None,
                                                                                       'skip': None, 'error': None,
                                                                                       'disabled': 0,
                                                                                       'duration_sec': 0, 'retries': 0,
                                                                                       'test_value1': 'test1'
                                                                                       }}


def test_object_custom_values():
    test_results_dto = TestResultDTO()
    custom_values = {
        "fields": {
            "test_result": {
                'test_value1': 'test1'
            },
            "suite_result": {
            }
        },
        "tags": {
            "test_result": {
                'test_value2': 'test2',
                'test_value3': 'test3'
            },
            "suite_result": {
            }
        }
    }
    test_results_dto.collect_custom_values(custom_values)
    assert test_results_dto.get_test_json('test_measurement_name')[0] == {'measurement': 'test_measurement_name',
                                                                          'tags': {'test': None, 'run': 'UNDEFINED',
                                                                                   'project': 'UNDEFINED',
                                                                                   'version': 'UNDEFINED',
                                                                                   'status_tag': 'disabled'},
                                                                          'fields': {'screenshot': None,
                                                                                     'duration_sec': None,
                                                                                     'exception': None,
                                                                                     'merged': False}}


def test_collected_tests():
    collected_tests = {'passed': 'test1', 'skipped': 'test2', 'xfailed': ''}

    results_dict = DataManager().get_results_dict(collected_tests)

    assert results_dict['passed'] == 5
    assert results_dict['skipped'] == 5
    assert results_dict['disabled'] == 5
